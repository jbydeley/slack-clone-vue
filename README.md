# Vue + GraphQL Slack Clone

## Introduction
This is based on a [tutorial](https://www.youtube.com/watch?v=0MKJ7JbVnFc&list=PLN3n1USn4xlkdRlq3VZ1sT6SGW0-yajjL) from [Ben Awad](https://twitter.com/benawad97). His tutorial is top notch but it was written in React. I decided to take it and write it in Vue.

## Prerequisites 
- Node
- Yarn (optional but nice to have)
- [The Server portion](https://gitlab.com/jbydeley/slack-clone-server)

## Installation
1. `yarn` or `npm install`
1. `yarn serve` or `npm run serve`
1. Navigate to `http://localhost:8080/`