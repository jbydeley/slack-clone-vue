import Vue from "vue";
import { ApolloClient } from "apollo-client";
import { setContext } from "apollo-link-context";
import { ApolloLink } from "apollo-link";
import { HttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import VueApollo from "vue-apollo";
import SuiVue from "semantic-ui-vue";
import "semantic-ui-css/semantic.min.css";

import App from "./App.vue";
import router from "./router";

Vue.config.productionTip = false;
Vue.config.performance = true;
Vue.use(SuiVue);

const httpLink = new HttpLink({
  uri: "http://localhost:8081/graphql"
});

const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem("token");
  const refreshToken = localStorage.getItem("refreshToken");
  return {
    headers: {
      ...headers,
      "x-token": token,
      "x-refresh-token": refreshToken
    }
  };
});

const afterwareLink = new ApolloLink((operation, forward) => {
  return forward(operation).map(response => {
    const { response: { headers } } = operation.getContext();
    if (headers) {
      const token = headers.get("x-token");
      const refreshToken = headers.get("x-refresh-token");

      if (token) {
        localStorage.setItem("token", token);
      }

      if (refreshToken) {
        localStorage.setItem("refreshToken", refreshToken);
      }
    }

    return response;
  });
});

const apolloClient = new ApolloClient({
  link: afterwareLink.concat(authLink.concat(httpLink)),
  cache: new InMemoryCache()
});

Vue.use(VueApollo);

const apolloProvider = new VueApollo({
  defaultClient: apolloClient
});

new Vue({
  router,
  apolloProvider,
  render: h => h(App)
}).$mount("#app");
