import Vue from "vue";
import Router from "vue-router";
import decode from "jwt-decode";

import Home from "./views/Home.vue";
import Register from "./views/Register.vue";
import Login from "./views/Login.vue";
import CreateTeam from "./views/CreateTeam.vue";
import ViewTeam from "./views/ViewTeam.vue";

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/register",
      name: "register",
      component: Register
    },
    {
      path: "/login",
      name: "login",
      component: Login
    },
    {
      path: "/create-team",
      name: "create-team",
      component: CreateTeam,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/view-team",
      name: "view-team",
      component: ViewTeam,
      meta: {
        requiresAuth: true
      }
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    const token = localStorage.getItem("token");
    const refreshToken = localStorage.getItem("refreshToken");
    try {
      decode(token);
      decode(refreshToken);
    } catch (error) {
      next({
        path: "login",
        query: { redirect: to.fullPath }
      });
      return;
    }
  }

  next();
});
export default router;
